defmodule Servy.Plugins do
  @moduledoc """
  Support plugins.
  """

  require Logger

  alias Servy.Conn

  def track(%Conn{ status: 404, path: path} = conn) do
    if Mix.env != :test do
      Logger.warn "Warning: #{path} is on the loose!"
    end
    conn
  end

  def track(%Conn{} = conn), do: conn

  def rewrite_path(%Conn{ path: "/wildlife"} = conn) do
    %{ conn | path: "/wildthings" }
  end

  def rewrite_path(%Conn{} = conn), do: conn

  def log(%Conn{} = conn) do
    if Mix.env == :dev do
      Logger.info inspect(conn)
    end
    conn
  end
end
