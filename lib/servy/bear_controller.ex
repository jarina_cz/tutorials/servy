defmodule Servy.BearController do
  
  alias Servy.Wildthings
  alias Servy.Bear

  import Servy.View, only: [render: 3]

  def index(conn) do
    bears =
      Wildthings.list_bears()
      |> Enum.sort(&Bear.order_asc_by_name/2)

    render(conn, "index.eex", bears: bears)
  end

  def show(conn, %{"id" => id}) do
    bear = Wildthings.get_bear(id)

    render(conn, "show.eex", bear: bear)
  end

  def create(conn, %{"name" => name, "type" => type}) do
    %{ conn | status: 201,
              resp_body: "Created a #{type} bear named #{name}!" }
  end

  def delete(conn) do
    %{ conn | status: 403, resp_body: "Deleting a bear is forbidden!"}
  end

end
