defmodule Servy.PledgeController do
  import Servy.View

  def index(conn) do
    # Gets the recen pledges from cache
    pledges = Servy.PledgeServer.recent_pledges()

    render(conn, "recent_pledges.eex", pledges: pledges)
  end

  def new(conn) do
    render(conn, "new_pledge.eex")
  end

  def create(conn, %{"name" => name, "amount" => amount}) do
    # Sends the pledge to the external service and caches it
    Servy.PledgeServer.create_pledge(name, String.to_integer(amount))

    %{ conn | status: 200, resp_body: "#{name} pledged #{amount}!" }
  end

  def cache_form(conn) do
    cache_size = Servy.PledgeServer.get_cache_size()
    render(conn, "cache_form.html.eex", cache_size: cache_size)
  end

  def cache_change(conn, %{"amount" => amount}) do
    Servy.PledgeServer.set_cache_size(amount)
    %{ conn | status: 200, resp_body: "New cache size is #{amount}!" }
  end

end
