defmodule Servy.Handler do
  @moduledoc """
  Handles HTTP requests.
  """
  alias Servy.Conn
  alias Servy.BearController

  import Servy.Plugins, only: [rewrite_path: 1, log: 1, track: 1]
  import Servy.Parser, only: [parse: 1]

  @pages_path Path.expand("../../pages", __DIR__)

  @doc """
  Transforms the request into a response.
  """
  def handle(request) do
    request
    |> parse
    |> rewrite_path
    |> log
    |> route
    |> track
    |> format_response
  end

  def route(%Conn{method: "POST", path: "/pledges"} = conn) do
    Servy.PledgeController.create(conn, conn.params)
  end

  def route(%Conn{method: "GET", path: "/pledges/cache"} = conn) do
    Servy.PledgeController.cache_form(conn)
  end

  def route(%Conn{method: "POST", path: "/pledges/cache"} = conn) do
    Servy.PledgeController.cache_change(conn, conn.params)
  end

  def route(%Conn{method: "GET", path: "/pledges"} = conn) do
    Servy.PledgeController.index(conn)
  end

  def route(%Conn{method: "GET", path: "/pledges/new"} = conn) do
    Servy.PledgeController.new(conn)
  end

  def route(%Conn{ method: "GET", path: "/snapshots" } = conn) do
    sensor_data = Servy.SensorServer.get_sensor_data()

    %{ conn | status: 200, resp_body: inspect sensor_data}
  end

  def route(%Conn{method: "GET", path: "/wildthings"} = conn) do
    %{ conn | status: 200, resp_body: "Bears, Lions, Tigers" }
  end

  def route(%Conn{method: "GET", path: "/api/bears"} = conn) do
    Servy.Api.BearController.index(conn)
  end

  def route(%Conn{method: "POST", path: "/api/bears"} = conn) do
    Servy.Api.BearController.create(conn, conn.params)
  end

  def route(%Conn{method: "GET", path: "/bears"} = conn) do
    BearController.index(conn)
  end

  def route(%Conn{method: "GET", path: "/bears/new"} = conn) do
    @pages_path
    |> Path.join("form.html")
    |> File.read
    |> handle_file(conn)
  end

  def route(%Conn{method: "GET", path: "/bears/" <> id} = conn) do
    params = Map.put(conn.params, "id", id)
    BearController.show(conn, params)
  end

  def route(%Conn{method: "DELETE", path: "/bears/" <> _id} = conn) do
    BearController.delete(conn)
  end

  def route(%Conn{method: "POST", path: "/bears"} = conn) do
    BearController.create(conn, conn.params)
  end

  def route(%Conn{method: "GET", path: "/about"} = conn) do
    @pages_path
    |> Path.join("about.html")
    |> File.read
    |> handle_file(conn)
  end

  def route(%Conn{path: path} = conn) do
    %{ conn | status: 404, resp_body: "No #{path} here!" }
  end

  def handle_file({:ok, content}, conn) do
    %{ conn | status: 200, resp_body: content}
  end

  def handle_file({:error, :enoent}, conn) do
    %{ conn | status: 404, resp_body: "File not found"}
  end

  def handle_file({:error, reason}, conn) do
    %{ conn | status: 500, resp_body: "File error: #{reason}"}
  end

  def format_response(%Conn{} = conn) do
    """
    HTTP/1.1 #{Conn.full_status(conn)}
    Content-Type: #{conn.resp_headers["Content-Type"]}
    Content-Length: #{String.length(conn.resp_body)}

    #{conn.resp_body}
    """
  end

end
