defmodule Servy.Api.BearController do
  
  def index(conn) do
    json =
      Servy.Wildthings.list_bears()
      |> Poison.encode!

    conn = put_resp_content_type(conn, "application/json")
    %{ conn | status: 200, resp_body: json }
  end

  def create(conn, %{"name" => name, "type" => type}) do
    %{ conn | status: 201,
      resp_body: "Created a #{type} bear named #{name}!" }
  end

  defp put_resp_content_type(conn, content_type) do
    new_headers = Map.put(conn.resp_headers, "Content-Type", content_type)
    %{ conn | resp_headers: new_headers }
  end

end
